<?php
declare(strict_types=1);
namespace App\Repository;
use App\Entity\Article;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;

class ArticleRepository
{
    private $connection;
    private static $MAPPING = [
        'id' => ['method' => 'setId', 'type' => 'int'],
        'text' => ['method' => 'setText', 'type' => 'string'],
        'created_at' => ['method' => 'setDate', 'type' => DateTime::class]
    ];

    /**
     * ArticleRepository constructor. ①
     *
     * @param $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }
    public function findAll(): ArrayCollection
    {
        $sqlArticleList = "SELECT * FROM articles";
        return $this->hydrateAllData($this->connection->fetchAll($sqlArticleList));
    }
    /**
     * Le nom de la méthode est tiré de celui présent dans AbstractHydrator de
    Doctrine ORM.
     * Le prototype n'est cependant pas respecté.
     *
     * @param $rows
     *
     * @return ArrayCollection
     */
    protected function hydrateAllData(array $rows): ArrayCollection
      {
      $articles = new ArrayCollection();
      foreach ($rows as $row) {
      $articles->add($this->hydrateRowData($row));
      }
    return $articles;
}
    protected function hydrateRowData(array $row): Article
      {
      $articles = new Article();
      foreach (self::$MAPPING as $fieldName => $fieldMetadata) {
      if (array_key_exists($fieldName, $row) && method_exists($articles,
    $fieldMetadata['method'])) {
      if ($fieldMetadata['type'] == DateTime::class) {
      call_user_func(array($articles, $fieldMetadata['method']), new
    DateTime($row[$fieldName]));
      } else {
        call_user_func(array($articles, $fieldMetadata['method']), $row
        [$fieldName]);
        }
        }
    }
    return $articles;
    }
}

