<?php
namespace App\Service;
use App\Repository\ArticleRepository;
use Doctrine\DBAL\Connection;
use Doctrine\Common\Collections\ArrayCollection;

class ArticleService
{
    protected $connection;
    protected $articleRepository;
    protected $blackListedExpressions = ['BAD WORD'];

    public function __construct(Connection $connection, ArticleRepository
    $articleRepository) {
        $this->connection = $connection;
        $this->articleRepository = $articleRepository;
    }

    public function findAllCleanedArticle() {
        $articleList = new ArrayCollection();

        foreach ($this->articleRepository->findAll() as $article) {
            $article->setText(
                str_replace($this->blackListedExpressions, '******', $article->
                getText())
            );
            $articleList->add($article);
        }
        return $articleList;
    }
}
