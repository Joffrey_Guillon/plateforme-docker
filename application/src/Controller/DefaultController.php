<?php

namespace App\Controller;
use App\Service\ArticleService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    private $articleService;

    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
    }
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $articles = $this->articleService->findAllCleanedArticle();
        return $this->render('default/home.html.twig', array(
            'articleList' => $articles,
        ));
    }
}
