<?php
namespace App\Tests\Controller;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
class DefaultControllerTest extends WebTestCase
{
    private $connection = null;
    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
        $kernel = self::bootKernel();
        $this->connection = $kernel->getContainer()
            ->get('database_connection');
        $this->connection->query('TRUNCATE articles;');
        $this->connection->query('INSERT INTO articles (text, created_at) VALUES
        ("article de test", NOW());');
    }
    /**
     * @inheritdoc
     */
    public function tearDown()
    {
        parent::tearDown();
        $this->connection->query('TRUNCATE articles;');
    }
    /**
     * @test
     */
    public function shouldReturnTestArticle()
    {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/');
        $this->assertTrue($client->getResponse()->isOk());
        $this->assertCount(1, $crawler->filter('p:contains("article de test")'));
    }
}
