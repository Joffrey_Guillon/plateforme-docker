<?php

namespace App\Tests\Repository;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
class ArticleRepositoryTest extends KernelTestCase
{
    /**
     * Connexion à la base de données.
     *
     * @var Doctrine\DBAL\Connection
     */
    private $connection;
    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        parent::setUp();
        $kernel = self::bootKernel();
        $this->connection = $kernel->getContainer()
            ->get('database_connection');
        $this->connection->query("INSERT INTO articles (text, created_at) VALUES (
\"Article de test.\", NOW());");
    }
    /**
     * {@inheritDoc}
     */
    public function tearDown()
    {
        parent::tearDown();
        $this->connection->query("TRUNCATE articles");
          $this->connection->close();
          $this->connection = null;
    }
    /**
     * @test
     */
    public function shouldHaveArticle()
    {
        $articleRepository = new ArticleRepository($this->connection);
        $this->assertCount(1, $articleRepository->findAll());
    }
}